﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiUpdaterScript : MonoBehaviour
{
    private Text _scoreText;
    void Start()
    {
        _scoreText = GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        _scoreText.text = $"Score: {GetComponentInParent<GameManagerScript>().Score}";
    }
}
