﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{
    private int _score;

    public int Score { get => _score; set => _score = value; }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }
}
