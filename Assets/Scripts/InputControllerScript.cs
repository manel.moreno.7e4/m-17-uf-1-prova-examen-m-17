﻿using UnityEngine;
using UnityEngine.InputSystem;

public class InputControllerScript : MonoBehaviour
{
    private InputAction _rightAction;
    private InputAction _leftAction;
    private InputAction _upAction;
    private InputAction _downAction;
    private InputAction _shootAction;

    private void Awake()
    {
        _rightAction = new InputAction();
        _rightAction.AddBinding(Keyboard.current.rightArrowKey);
        _rightAction.performed += OnRight;
        _rightAction.canceled += OnCancelMovement;
        _rightAction.Enable();

        _leftAction = new InputAction();
        _leftAction.AddBinding(Keyboard.current.leftArrowKey);
        _leftAction.performed += OnLeft;
        _leftAction.canceled += OnCancelMovement;
        _leftAction.Enable();

        _upAction = new InputAction();
        _upAction.AddBinding(Keyboard.current.upArrowKey);
        _upAction.performed += OnUp;
        _upAction.canceled += OnCancelMovement;
        _upAction.Enable();

        _downAction = new InputAction();
        _downAction.AddBinding(Keyboard.current.downArrowKey);
        _downAction.performed += OnDown;
        _downAction.canceled += OnCancelMovement;
        _downAction.Enable();

        _shootAction = new InputAction();
        _shootAction.AddBinding(Keyboard.current.spaceKey);
        _shootAction.performed += OnShoot;
        //_downAction.canceled += OnCancelMovement;
        _shootAction.Enable();
    }


    protected virtual void OnUp(InputAction.CallbackContext obj)
    {

    }

    protected virtual void OnRight(InputAction.CallbackContext obj)
    {

    }

    protected virtual void OnCancelMovement(InputAction.CallbackContext obj)
    {

    }

    protected virtual void OnLeft(InputAction.CallbackContext obj)
    {

    }

    protected virtual void OnDown(InputAction.CallbackContext obj)
    {

    }

    protected virtual void OnShoot(InputAction.CallbackContext obj)
    {

    }

}
