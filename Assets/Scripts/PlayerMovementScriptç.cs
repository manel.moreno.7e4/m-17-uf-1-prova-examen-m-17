﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovementScriptç : InputControllerScript
{
    [SerializeField] private float _speed;
    protected override void OnRight(InputAction.CallbackContext obj)
    {
        transform.position = new Vector3(transform.position.x + 1 * _speed, transform.position.y, transform.position.z);
        StartCoroutine(UtilitiesScript.ExecuteAfterTime(() => OnRight(obj), 0));
    }

    protected override void OnLeft(InputAction.CallbackContext obj)
    {
        transform.position = new Vector3(transform.position.x - 1 * _speed, transform.position.y, transform.position.z);
        StartCoroutine(UtilitiesScript.ExecuteAfterTime(() => OnLeft(obj), 0));
    }

    protected override void OnUp(InputAction.CallbackContext obj)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + 1 * _speed, transform.position.z);
        StartCoroutine(UtilitiesScript.ExecuteAfterTime(() => OnUp(obj), 0));
    }

    protected override void OnDown(InputAction.CallbackContext obj)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - 1 * _speed, transform.position.z);
        StartCoroutine(UtilitiesScript.ExecuteAfterTime(() => OnDown(obj), 0));
    }

    protected override void OnCancelMovement(InputAction.CallbackContext obj)
    {
        StopAllCoroutines();
    }
}
