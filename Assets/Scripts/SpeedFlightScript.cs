﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedFlightScript : MonoBehaviour
{
    [SerializeField] private float _speed;
    private void UpDownMovement()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - 1 * _speed, transform.position.z);
    }

    private void Update()
    {
        //UpDownMovement();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<BulletBehaviourScript>())
        {
            GetComponentInParent<GameManagerScript>().Score += 5;
        }
        Destroy(gameObject);
    }
}
