﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateScript : MonoBehaviour
{
    [SerializeField] private Transform _spawnPos1;
    [SerializeField] private Transform _spawnPos2;
    [SerializeField] private GameObject _bulletFragatePrefab;

    private byte _hits;

    private void Start()
    {
        ShootRoutine();
    }

    private void ShootRoutine()
    {
        OneShot();

        StartCoroutine(UtilitiesScript.ExecuteAfterTime(OneShot, 0.2f));

        StartCoroutine(UtilitiesScript.ExecuteAfterTime(OneShot, 0.4f));

        StartCoroutine(UtilitiesScript.ExecuteAfterTime(ShootRoutine, 2f));

    }

    private void OneShot()
    {
        var inst1 = Instantiate(_bulletFragatePrefab, _spawnPos1.transform.position, Quaternion.identity);
        inst1.transform.parent = GetComponentInParent<GameManagerScript>().transform;
        var inst2 = Instantiate(_bulletFragatePrefab, _spawnPos2.transform.position, Quaternion.identity);
        inst2.transform.parent = GetComponentInParent<GameManagerScript>().transform;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("DownBorderController"))
        {
            Destroy(gameObject);
        }

        if(collision.gameObject.GetComponent<BulletBehaviourScript>())
        {
            _hits++;
            if(_hits>=5)
            {
                GetComponentInParent<GameManagerScript>().Score += 20;
                Destroy(gameObject);
            }
        }
    }
}
