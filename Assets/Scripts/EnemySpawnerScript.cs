﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerScript : MonoBehaviour
{
    [SerializeField] private GameObject[] _enemies;
    [SerializeField] private Transform _minX;
    [SerializeField] private Transform _maxX;
    private void Start()
    {
        SpawnEnemie();
    }

    private void SpawnEnemie()
    {
        var enemie = Instantiate(_enemies[Random.Range(0, 2)], new Vector3(Random.Range(_minX.position.x, _maxX.position.x), transform.position.y, transform.position.z), Quaternion.identity);
        enemie.transform.parent = GetComponentInParent<GameManagerScript>().transform;
    }
}
