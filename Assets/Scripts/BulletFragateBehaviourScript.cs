﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFragateBehaviourScript : MonoBehaviour
{
    private float _speed = 0.1f;
    void Update()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y - 1 * _speed, transform.position.z);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<PlayerMovementScriptç>())
        {
            GetComponentInParent<GameManagerScript>().Restart();
        }
        
    }
}
