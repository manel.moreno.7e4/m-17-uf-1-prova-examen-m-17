﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShootController : InputControllerScript
{
    [SerializeField] private GameObject _bulletPrefab;

    [SerializeField] private Transform _spawnPos1;
    [SerializeField] private Transform _spawnPos2;

    protected override void OnShoot(InputAction.CallbackContext obj)
    {
        Instantiate(_bulletPrefab, _spawnPos1.transform.position, Quaternion.identity);
        Instantiate(_bulletPrefab, _spawnPos2.transform.position, Quaternion.identity);
    }
}
