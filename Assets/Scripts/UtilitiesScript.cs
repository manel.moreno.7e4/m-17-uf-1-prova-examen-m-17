﻿using System;
using System.Collections;
using System.Reflection;
using UnityEngine;

public class UtilitiesScript : MonoBehaviour
{
    private static IEnumerator _executeOnceDelayRoutine;

    /// <summary>
    /// Use it on an update to execute something once, and then wait the cooldown time to be able to execute it again
    /// </summary>
    /// <Implementation>
    /// boolController = UtilitiesScript.ExecuteOnceAndWaitCd(MethodName, boolController, coolDownTime);
    /// </Implementation>  
    // boolController = UtilitiesScript.ExecuteOnceAndWaitCd(MethodName, boolController, coolDownTime);
    /// <param name="actionToExecute"> void Method passed as parameter with the action to execute</param>
    /// <param name="boolController"> the boolean flag necessary to get all this sequence working</param>
    /// <param name="cooldownTime"> float determining the time in seconds we wait until we can execute again the sequence</param>
    /// <returns>boolController(see implementation)</returns>
    public static bool ExecuteOnceAndWaitCd(Action actionToExecute, bool boolController, float cooldownTime)
    {
        ExecuteAnyway(actionToExecute, boolController);
        boolController = ReactivateAfterCd(cooldownTime);
        return boolController;
    }

    //Method helper to ExecuteOnceAndWaitCd()
    private static bool ExecuteAnyway(Action actionToExecute, bool boolController)
    {
        if (boolController)
        {
            return false;
        }
        actionToExecute.Invoke();
        return true;
    }

    //Method helper to ExecuteOnceAndWaitCd()
    private static bool ReactivateAfterCd(float cooldownTime)
    {
        _executeOnceDelayRoutine = ReactivateAfterCdRoutine(cooldownTime);
        if (_executeOnceDelayRoutine == null)
        {
            return false;
        }
        return true;
    }

    //Method helper to ExecuteOnceAndWaitCd()
    private static IEnumerator ReactivateAfterCdRoutine(float cooldownTime)
    {
        yield return new WaitForSeconds(cooldownTime);
        _executeOnceDelayRoutine = null;
    }

    /// <summary>
    /// Use it on an update to execute something once
    /// </summary>
    /// <Implementation>
    ///  boolController = UtilitiesScript.JustExecuteOnce(MethodName, boolController)
    /// </Implementation>
    /// <param name="actionToExecute">void Method passed as parameter with the action to execute</param>
    /// <param name="boolController">the boolean flag necessary to get all this sequence working</param>
    /// <returns>boolController(see implementation)</returns>
    public static bool JustExecuteOnce(Action actionToExecute, bool boolController)
    {
        if (!boolController)
        {
            actionToExecute.Invoke();
        }
        return true;
    }

    /// <summary>
    /// Same as above but passing Coroutines as parameter instead of methods
    /// </summary>
    /// <param name="coroutine"></param>
    /// <param name="boolController"></param>
    /// <returns></returns>
    public bool JustExecuteOnce(IEnumerator coroutine, bool boolController)
    {
        if (!boolController)
        {
            StartCoroutine(coroutine);
        }
        return true;
    }

    /// <summary>
    /// Execute something after a certain time
    /// </summary>
    /// <Implementation>
    /// StartCoroutine(UtilitiesScript.ExecuteAfterTime(MethodName, timeToWait));
    /// </Implementation>
    /// <param name="actionToExecute">method passed as parameter with the action to execute</param>
    /// <param name="time"> time to wait before executiong the actionToExecute
    /// <returns></returns>
    public static IEnumerator ExecuteAfterTime(Action actionToExecute, float time)
    {
        yield return new WaitForSeconds(time);
        actionToExecute.Invoke();
    }

    /// <summary>
    /// To clear the log console of Debug.Log() messages
    /// </summary>
    /// <Implementation>
    /// UtilitiesScript.ClearLogConsole()
    /// </Implementation>


    //public static void ClearLogConsole()
    //{
    //    var assembly = Assembly.GetAssembly(typeof(UnityEditorInternal.EditMode));
    //    var type = assembly.GetType("UnityEditor.LogEntries");
    //    var method = type.GetMethod("Clear");
    //    method.Invoke(new object(), null);
    //}
}

